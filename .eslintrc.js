module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/recommended'
    ],
    parserOptions: {
        parser: 'babel-eslint',
    },
    rules: {
        "no-unused-vars": "off",
        "no-mixed-spaces-and-tabs": 0
    },
};
