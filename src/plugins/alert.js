import XAlert from '../components/XAlert.vue';

export default {
    install(Vue, args = {}) {
        if (this.installed) return;

        this.installed = true;
        this.params = args;

        Vue.component('x-alert', XAlert);

        const alert = params => {
            window.EventBus.$emit('open', params);
        }

        Vue.prototype.$alert = alert;
        Vue['$alert'] = alert;
    }
}
