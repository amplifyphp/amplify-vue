import add-outline from './add-outline.vue';
import add-solid from './add-solid.vue';
import adjust from './adjust.vue';
import airplane from './airplane.vue';
import album from './album.vue';
import align-center from './align-center.vue';
import align-justified from './align-justified.vue';
import align-left from './align-left.vue';
import align-right from './align-right.vue';
import anchor from './anchor.vue';
import announcement from './announcement.vue';
import apparel from './apparel.vue';
import arrow-down from './arrow-down.vue';
import arrow-left from './arrow-left.vue';
import arrow-outline-down from './arrow-outline-down.vue';
import arrow-outline-left from './arrow-outline-left.vue';
import arrow-outline-right from './arrow-outline-right.vue';
import arrow-outline-up from './arrow-outline-up.vue';
import arrow-right from './arrow-right.vue';
import arrow-thick-down from './arrow-thick-down.vue';
import arrow-thick-left from './arrow-thick-left.vue';
import arrow-thick-right from './arrow-thick-right.vue';
import arrow-thick-up from './arrow-thick-up.vue';
import arrow-thin-down from './arrow-thin-down.vue';
import arrow-thin-left from './arrow-thin-left.vue';
import arrow-thin-right from './arrow-thin-right.vue';
import arrow-thin-up from './arrow-thin-up.vue';
import arrow-up from './arrow-up.vue';
import artist from './artist.vue';
import at-symbol from './at-symbol.vue';
import attachment from './attachment.vue';
import backspace from './backspace.vue';
import backward from './backward.vue';
import backward-step from './backward-step.vue';
import badge from './badge.vue';
import battery-full from './battery-full.vue';
import battery-half from './battery-half.vue';
import battery-low from './battery-low.vue';
import beverage from './beverage.vue';
import block from './block.vue';
import bluetooth from './bluetooth.vue';
import bolt from './bolt.vue';
import bookmark from './bookmark.vue';
import bookmark-outline from './bookmark-outline.vue';
import bookmark-outline-add from './bookmark-outline-add.vue';
import book-reference from './book-reference.vue';
import border-all from './border-all.vue';
import border-bottom from './border-bottom.vue';
import border-horizontal from './border-horizontal.vue';
import border-inner from './border-inner.vue';
import border-left from './border-left.vue';
import border-none from './border-none.vue';
import border-outer from './border-outer.vue';
import border-right from './border-right.vue';
import border-top from './border-top.vue';
import border-vertical from './border-vertical.vue';
import box from './box.vue';
import brightness-down from './brightness-down.vue';
import brightness-up from './brightness-up.vue';
import browser-window from './browser-window.vue';
import browser-window-new from './browser-window-new.vue';
import browser-window-open from './browser-window-open.vue';
import bug from './bug.vue';
import buoy from './buoy.vue';
import calculator from './calculator.vue';
import calendar from './calendar.vue';
import camera from './camera.vue';
import chart from './chart.vue';
import chart-bar from './chart-bar.vue';
import chart-pie from './chart-pie.vue';
import chat-bubble-dots from './chat-bubble-dots.vue';
import checkmark from './checkmark.vue';
import checkmark-outline from './checkmark-outline.vue';
import cheveron-down from './cheveron-down.vue';
import cheveron-left from './cheveron-left.vue';
import cheveron-outline-down from './cheveron-outline-down.vue';
import cheveron-outline-left from './cheveron-outline-left.vue';
import cheveron-outline-right from './cheveron-outline-right.vue';
import cheveron-outline-up from './cheveron-outline-up.vue';
import cheveron-right from './cheveron-right.vue';
import cheveron-up from './cheveron-up.vue';
import clipboard from './clipboard.vue';
import close from './close.vue';
import close-outline from './close-outline.vue';
import close-solid from './close-solid.vue';
import cloud from './cloud.vue';
import cloud-upload from './cloud-upload.vue';
import code from './code.vue';
import coffee from './coffee.vue';
import cog from './cog.vue';
import color-palette from './color-palette.vue';
import compose from './compose.vue';
import computer-desktop from './computer-desktop.vue';
import computer-laptop from './computer-laptop.vue';
import conversation from './conversation.vue';
import copy from './copy.vue';
import credit-card from './credit-card.vue';
import currency-dollar from './currency-dollar.vue';
import dashboard from './dashboard.vue';
import date-add from './date-add.vue';
import dial-pad from './dial-pad.vue';
import directions from './directions.vue';
import document from './document.vue';
import document-add from './document-add.vue';
import dots-horizontal-double from './dots-horizontal-double.vue';
import dots-horizontal-triple from './dots-horizontal-triple.vue';
import download from './download.vue';
import duplicate from './duplicate.vue';
import edit-copy from './edit-copy.vue';
import edit-crop from './edit-crop.vue';
import edit-cut from './edit-cut.vue';
import edit-pencil from './edit-pencil.vue';
import education from './education.vue';
import envelope from './envelope.vue';
import exclamation-outline from './exclamation-outline.vue';
import exclamation-solid from './exclamation-solid.vue';
import explore from './explore.vue';
import factory from './factory.vue';
import fast-forward from './fast-forward.vue';
import fast-rewind from './fast-rewind.vue';
import film from './film.vue';
import filter-icon from './filter-icon.vue';
import flag from './flag.vue';
import flashlight from './flashlight.vue';
import folder from './folder.vue';
import folder-outline from './folder-outline.vue';
import folder-outline-add from './folder-outline-add.vue';
import format-bold from './format-bold.vue';
import format-font-size from './format-font-size.vue';
import format-italic from './format-italic.vue';
import format-text-size from './format-text-size.vue';
import format-underline from './format-underline.vue';
import forward from './forward.vue';
import forward-step from './forward-step.vue';
import gift from './gift.vue';
import globe from './globe.vue';
import hand-stop from './hand-stop.vue';
import hard-drive from './hard-drive.vue';
import headphones from './headphones.vue';
import heart from './heart.vue';
import home from './home.vue';
import hot from './hot.vue';
import hour-glass from './hour-glass.vue';
import inbox from './inbox.vue';
import inbox-check from './inbox-check.vue';
import inbox-download from './inbox-download.vue';
import inbox-full from './inbox-full.vue';
import indent-decrease from './indent-decrease.vue';
import indent-increase from './indent-increase.vue';
import information-outline from './information-outline.vue';
import information-solid from './information-solid.vue';
import key from './key.vue';
import keyboard from './keyboard.vue';
import layers from './layers.vue';
import library from './library.vue';
import light-bulb from './light-bulb.vue';
import link from './link.vue';
import list from './list.vue';
import list-add from './list-add.vue';
import list-bullet from './list-bullet.vue';
import load-balancer from './load-balancer.vue';
import location from './location.vue';
import location-current from './location-current.vue';
import location-food from './location-food.vue';
import location-gas-station from './location-gas-station.vue';
import location-hotel from './location-hotel.vue';
import location-marina from './location-marina.vue';
import location-park from './location-park.vue';
import location-restroom from './location-restroom.vue';
import location-shopping from './location-shopping.vue';
import lock-closed from './lock-closed.vue';
import lock-open from './lock-open.vue';
import map from './map.vue';
import menu from './menu.vue';
import mic from './mic.vue';
import minus-outline from './minus-outline.vue';
import minus-solid from './minus-solid.vue';
import mobile-devices from './mobile-devices.vue';
import mood-happy-outline from './mood-happy-outline.vue';
import mood-happy-solid from './mood-happy-solid.vue';
import mood-neutral-outline from './mood-neutral-outline.vue';
import mood-neutral-solid from './mood-neutral-solid.vue';
import mood-sad-outline from './mood-sad-outline.vue';
import mood-sad-solid from './mood-sad-solid.vue';
import mouse from './mouse.vue';
import music-album from './music-album.vue';
import music-artist from './music-artist.vue';
import music-notes from './music-notes.vue';
import music-playlist from './music-playlist.vue';
import navigation-more from './navigation-more.vue';
import network from './network.vue';
import news-paper from './news-paper.vue';
import notification from './notification.vue';
import notifications from './notifications.vue';
import notifications-outline from './notifications-outline.vue';
import paste from './paste.vue';
import pause from './pause.vue';
import pause-outline from './pause-outline.vue';
import pause-solid from './pause-solid.vue';
import pen-tool from './pen-tool.vue';
import phone from './phone.vue';
import photo from './photo.vue';
import php-elephant from './php-elephant.vue';
import pin from './pin.vue';
import play from './play.vue';
import playlist from './playlist.vue';
import play-outline from './play-outline.vue';
import plugin from './plugin.vue';
import portfolio from './portfolio.vue';
import printer from './printer.vue';
import pylon from './pylon.vue';
import question from './question.vue';
import queue from './queue.vue';
import radar from './radar.vue';
import radio from './radio.vue';
import refresh from './refresh.vue';
import reload from './reload.vue';
import reply from './reply.vue';
import reply-all from './reply-all.vue';
import repost from './repost.vue';
import save-disk from './save-disk.vue';
import screen-full from './screen-full.vue';
import search from './search.vue';
import send from './send.vue';
import servers from './servers.vue';
import share from './share.vue';
import share-01 from './share-01.vue';
import share-alt from './share-alt.vue';
import shield from './shield.vue';
import shopping-cart from './shopping-cart.vue';
import show-sidebar from './show-sidebar.vue';
import shuffle from './shuffle.vue';
import stand-by from './stand-by.vue';
import star-full from './star-full.vue';
import station from './station.vue';
import step-backward from './step-backward.vue';
import step-forward from './step-forward.vue';
import stethoscope from './stethoscope.vue';
import store-front from './store-front.vue';
import stroke-width from './stroke-width.vue';
import subdirectory-left from './subdirectory-left.vue';
import subdirectory-right from './subdirectory-right.vue';
import swap from './swap.vue';
import tablet from './tablet.vue';
import tag from './tag.vue';
import target from './target.vue';
import text-box from './text-box.vue';
import text-decoration from './text-decoration.vue';
import thermometer from './thermometer.vue';
import thumbs-down from './thumbs-down.vue';
import thumbs-up from './thumbs-up.vue';
import ticket from './ticket.vue';
import time from './time.vue';
import timer from './timer.vue';
import tools from './tools.vue';
import translate from './translate.vue';
import trash from './trash.vue';
import travel from './travel.vue';
import travel-bus from './travel-bus.vue';
import travel-car from './travel-car.vue';
import travel-case from './travel-case.vue';
import travel-taxi-cab from './travel-taxi-cab.vue';
import travel-train from './travel-train.vue';
import travel-walk from './travel-walk.vue';
import trophy from './trophy.vue';
import tuning from './tuning.vue';
import upload from './upload.vue';
import usb from './usb.vue';
import user from './user.vue';
import user-add from './user-add.vue';
import user-group from './user-group.vue';
import user-solid-circle from './user-solid-circle.vue';
import user-solid-square from './user-solid-square.vue';
import vector from './vector.vue';
import video-camera from './video-camera.vue';
import view-carousel from './view-carousel.vue';
import view-column from './view-column.vue';
import view-hide from './view-hide.vue';
import view-list from './view-list.vue';
import view-show from './view-show.vue';
import view-tile from './view-tile.vue';
import volume-down from './volume-down.vue';
import volume-mute from './volume-mute.vue';
import volume-off from './volume-off.vue';
import volume-up from './volume-up.vue';
import wallet from './wallet.vue';
import watch from './watch.vue';
import window from './window.vue';
import window-new from './window-new.vue';
import window-open from './window-open.vue';
import wrench from './wrench.vue';
import yin-yang from './yin-yang.vue';
import zoom-in from './zoom-in.vue';
import zoom-out from './zoom-out.vue';

export default {
  add-outline,
  add-solid,
  adjust,
  airplane,
  album,
  align-center,
  align-justified,
  align-left,
  align-right,
  anchor,
  announcement,
  apparel,
  arrow-down,
  arrow-left,
  arrow-outline-down,
  arrow-outline-left,
  arrow-outline-right,
  arrow-outline-up,
  arrow-right,
  arrow-thick-down,
  arrow-thick-left,
  arrow-thick-right,
  arrow-thick-up,
  arrow-thin-down,
  arrow-thin-left,
  arrow-thin-right,
  arrow-thin-up,
  arrow-up,
  artist,
  at-symbol,
  attachment,
  backspace,
  backward,
  backward-step,
  badge,
  battery-full,
  battery-half,
  battery-low,
  beverage,
  block,
  bluetooth,
  bolt,
  bookmark,
  bookmark-outline,
  bookmark-outline-add,
  book-reference,
  border-all,
  border-bottom,
  border-horizontal,
  border-inner,
  border-left,
  border-none,
  border-outer,
  border-right,
  border-top,
  border-vertical,
  box,
  brightness-down,
  brightness-up,
  browser-window,
  browser-window-new,
  browser-window-open,
  bug,
  buoy,
  calculator,
  calendar,
  camera,
  chart,
  chart-bar,
  chart-pie,
  chat-bubble-dots,
  checkmark,
  checkmark-outline,
  cheveron-down,
  cheveron-left,
  cheveron-outline-down,
  cheveron-outline-left,
  cheveron-outline-right,
  cheveron-outline-up,
  cheveron-right,
  cheveron-up,
  clipboard,
  close,
  close-outline,
  close-solid,
  cloud,
  cloud-upload,
  code,
  coffee,
  cog,
  color-palette,
  compose,
  computer-desktop,
  computer-laptop,
  conversation,
  copy,
  credit-card,
  currency-dollar,
  dashboard,
  date-add,
  dial-pad,
  directions,
  document,
  document-add,
  dots-horizontal-double,
  dots-horizontal-triple,
  download,
  duplicate,
  edit-copy,
  edit-crop,
  edit-cut,
  edit-pencil,
  education,
  envelope,
  exclamation-outline,
  exclamation-solid,
  explore,
  factory,
  fast-forward,
  fast-rewind,
  film,
  filter-icon,
  flag,
  flashlight,
  folder,
  folder-outline,
  folder-outline-add,
  format-bold,
  format-font-size,
  format-italic,
  format-text-size,
  format-underline,
  forward,
  forward-step,
  gift,
  globe,
  hand-stop,
  hard-drive,
  headphones,
  heart,
  home,
  hot,
  hour-glass,
  inbox,
  inbox-check,
  inbox-download,
  inbox-full,
  indent-decrease,
  indent-increase,
  information-outline,
  information-solid,
  key,
  keyboard,
  layers,
  library,
  light-bulb,
  link,
  list,
  list-add,
  list-bullet,
  load-balancer,
  location,
  location-current,
  location-food,
  location-gas-station,
  location-hotel,
  location-marina,
  location-park,
  location-restroom,
  location-shopping,
  lock-closed,
  lock-open,
  map,
  menu,
  mic,
  minus-outline,
  minus-solid,
  mobile-devices,
  mood-happy-outline,
  mood-happy-solid,
  mood-neutral-outline,
  mood-neutral-solid,
  mood-sad-outline,
  mood-sad-solid,
  mouse,
  music-album,
  music-artist,
  music-notes,
  music-playlist,
  navigation-more,
  network,
  news-paper,
  notification,
  notifications,
  notifications-outline,
  paste,
  pause,
  pause-outline,
  pause-solid,
  pen-tool,
  phone,
  photo,
  php-elephant,
  pin,
  play,
  playlist,
  play-outline,
  plugin,
  portfolio,
  printer,
  pylon,
  question,
  queue,
  radar,
  radio,
  refresh,
  reload,
  reply,
  reply-all,
  repost,
  save-disk,
  screen-full,
  search,
  send,
  servers,
  share,
  share-01,
  share-alt,
  shield,
  shopping-cart,
  show-sidebar,
  shuffle,
  stand-by,
  star-full,
  station,
  step-backward,
  step-forward,
  stethoscope,
  store-front,
  stroke-width,
  subdirectory-left,
  subdirectory-right,
  swap,
  tablet,
  tag,
  target,
  text-box,
  text-decoration,
  thermometer,
  thumbs-down,
  thumbs-up,
  ticket,
  time,
  timer,
  tools,
  translate,
  trash,
  travel,
  travel-bus,
  travel-car,
  travel-case,
  travel-taxi-cab,
  travel-train,
  travel-walk,
  trophy,
  tuning,
  upload,
  usb,
  user,
  user-add,
  user-group,
  user-solid-circle,
  user-solid-square,
  vector,
  video-camera,
  view-carousel,
  view-column,
  view-hide,
  view-list,
  view-show,
  view-tile,
  volume-down,
  volume-mute,
  volume-off,
  volume-up,
  wallet,
  watch,
  window,
  window-new,
  window-open,
  wrench,
  yin-yang,
  zoom-in,
  zoom-out,
};
