import XIcon from './components/XIcon.vue';
import XInput from './components/XInput.vue';
import XMenu from './components/XMenu.vue';
import XModal from './components/XModal.vue';
import XTab from './components/XTab.vue';
import XTabs from './components/XTabs.vue';
import XTable from './components/XTable.vue';
import XSelect from './components/XSelect.vue';
import XToggle from './components/XToggle.vue';
import XUpload from './components/XUpload.vue';

import XAlert from './plugins/alert';
import XConfirm from './plugins/confirm';

export function install(Vue) {
    if (install.installed) return;

    install.installed = true;

    Vue.use(XAlert);
    Vue.use(XConfirm);

    Vue.component('x-icon', XIcon);
    Vue.component('x-input', XInput);
    Vue.component('x-modal', XModal);
    Vue.component('x-tab', XTab);
    Vue.component('x-tabs', XTabs);
    Vue.component('x-table', XTable);
    Vue.component('x-select', XSelect);
    Vue.component('x-toggle', XToggle);
    Vue.component('x-upload', XUpload);
    Vue.component('x-menu', XMenu);
}

// create module definition
const plugin = {
    install,
};

// auto install when vue is found
let GlobalVue = null;
if (typeof window !== 'undefined') {
    GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
    GlobalVue = global.Vue;
}

if (GlobalVue) {
    GlobalVue.use(plugin);
}

// to allow use as module
export default plugin;
