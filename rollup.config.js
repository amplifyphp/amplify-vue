import cjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import vue from 'rollup-plugin-vue';
import requireContext from 'rollup-plugin-require-context';

export default {
    input: 'src/index.js',
    output: {
        name: 'VueAmplify',
        exports: 'named',
        globals: {
            vue: 'Vue'
        },
    },
    plugins: [
        vue({
            css: true,
            compileTemplate: true,
        }),
        cjs(),
        requireContext(),
        resolve({
            mainFields: [
                'module', 'jsnext', 'index', 'browser'
            ],
        }),
    ],
    watch: {
        include: 'src/**',
    },
    external: [
        'vue',
        'axios',
        'v-click-outside',
        'lodash/includes',
        'lodash/findKey'
    ],
}
