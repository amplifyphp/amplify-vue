#!/bin/bash

BEGIN_SVG="<svg xmlns=\"http:\/\/www.w3.org\/2000\/svg\" viewBox=\"0 0 20 20\">"
END_SVG="</svg>"
BEGIN_TEMPLATE="<template>"
END_TEMPLATE="</template>"

spinal() {
    PASCAL=`echo "$1"`
}

rm -rf icons
rm -rf ../src/icons
mkdir ../src/icons

git clone https://github.com/dukestreetstudio/zondicons.git

cd zondicons
cd src

rm "bookmark copy 2.svg"
rm "bookmark copy 3.svg"
rm "radar copy 2.svg"
mv "tools copy.svg" "tools.svg"
mv "filter.svg" "filter-icon.svg"

for filename in *.svg; do
    base=${filename%.svg}
    sed -i -e "s|$BEGIN_SVG|$BEGIN_TEMPLATE|g" "$filename"
    sed -i -e "s|${END_SVG}|${END_TEMPLATE}|g" "$filename"
    spinal $base
    mv "$filename" "../../../src/icons/${PASCAL}.vue"
done

cd ../../../src/icons

INDEX="index.js"
touch $INDEX

for filename in *.vue; do
    base=${filename%.vue}
    echo "import ${base} from './${filename}';" >> $INDEX
done
echo  >> $INDEX
echo "export default {" >> $INDEX
for filename in *.vue; do
    base=${filename%.vue}
    echo "  ${base}," >> $INDEX
done

echo "};" >> $INDEX

# clean up
cd ../../scripts
pwd
# rm -rf icons
