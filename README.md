# Amplify-Vue

## Installation

```bash
npm install amplify-vue
```

## Usage

```bash
import Vue from 'vue';
import VueAmplify from 'amplify-vue';

Vue.use(VueAmplify);

```
